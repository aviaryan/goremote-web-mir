const ByTwitter = () => (
	<a
		href='https://twitter.com/aviaryan123'
		target='_blank'
		className={
			'fixed bottom-0 right-0 p-2 bg-white rounded-tl border-l border-t ' +
			'border-gray-700 cursor-pointer hidden lg:block shadow'
		}
	>
		<img
			className='rounded-full lazyload w-6 h-6 inline mr-2'
			data-src='https://s.gravatar.com/avatar/4ab40e6b657e0bc91bf4961450f96420?s=80'
			alt='Avi'
		/>
		by @aviaryan123
	</a>
)

export default ByTwitter
