const GoRemoteCompany = {
	name: 'GoRemote',
	website: 'https://goremote.in',
	logo: 'https://goremote.in/favicon.png',
}

export default function OrgSchema({ company }) {
	company = company || GoRemoteCompany
	return (
		<script
			key={`company-${company.name}`}
			type='application/ld+json'
			dangerouslySetInnerHTML={{ __html: JSON.stringify(makeOrgSchema(company)) }}
		/>
	)
}

function makeOrgSchema(company) {
	const schema = {
		'@context': 'https://schema.org',
		'@type': 'Organization',
		name: company.name,
	}

	if (company.website) {
		schema['url'] = company.website
		// some tidbit, structured data tool throws error if adding logo without URL
		if (company.logo) {
			schema['logo'] = company.logo
		}
	}

	return schema
}
