import { logJobApply } from '../utils/analytics'
import { getJobPostedLargeString } from '../lib/date'
import { attachRefToUrl } from '../lib/jobs'

export default function JobDetail({ data }) {
	return (
		<div>
			<div className='flex md:flex-row flex-col items-center'>
				<div>
					<img
						data-src={data.company.logo}
						alt={`${data.company.name} logo`}
						className='lazyload h-20 w-20 md:w-24 md:h-24 max-w-unset'
					/>
				</div>
				<h1 className='text-3xl pl-4'>
					<b>{data.title}</b> at {data.company.name}
				</h1>
			</div>
			<div className='mt-8'>
				<span role='img' aria-label='Clock'>
					🕑{' '}
				</span>
				{getJobPostedLargeString(data.postedAt)}
			</div>
			<div className='mt-8 job-detail' dangerouslySetInnerHTML={{ __html: data.description }} />
			<div className='mt-8'>
				<a
					href={attachRefToUrl(data.applyURL)}
					target='_blank'
					className='bg-blue-600 text-white px-3 py-2 rounded shadow'
					onClick={() => logJobApply(data.applyURL)}
				>
					Apply for this Job
				</a>
			</div>
		</div>
	)
}
