import { stripHTML } from '../lib/strings'
import { addDaysToDate } from '../lib/date'

export default function JobSchema({ job }) {
	return (
		<script
			key={`jobJSON-${job.id}`}
			type='application/ld+json'
			dangerouslySetInnerHTML={{ __html: JSON.stringify(makeJobSchema(job)) }}
		/>
	)
}

// https://developers.google.com/search/docs/data-types/job-posting

function makeJobSchema(job) {
	const desc = stripHTML(job.description)
	return {
		'@context': 'http://schema.org',
		'@type': 'JobPosting',
		datePosted: job.postedAt,
		description: desc,
		baseSalary: {
			'@type': 'MonetaryAmount',
			currency: 'USD',
			// TODO: add actual salary
			value: {
				'@type': 'QuantitativeValue',
				value: 3000,
				unitText: 'MONTH',
			},
		},
		// TODO: use actual employment type
		employmentType: 'FULL_TIME',
		industry: 'Startups',
		// applicantLocationRequirements: {
		// 	'@type': 'Country',
		// 	// not sure if keep this applicantRequirement thing
		// 	// can help with an edge in India searches
		// 	name: 'India',
		// },
		jobLocationType: 'TELECOMMUTE',
		jobLocation: {
			address: {
				'@type': 'PostalAddress',
				addressCountry: 'Anywhere',
				addressRegion: 'Anywhere',
				streetAddress: 'Anywhere',
				postalCode: 'Anywhere',
				addressLocality: 'Anywhere',
			},
		},
		title: job.title,
		image: job.company.logo,
		// occupationalCategory: '',
		workHours: 'Flexible',
		validThrough: addDaysToDate(job.postedAt, 60),
		hiringOrganization: {
			'@type': 'Organization',
			name: job.company.name,
			sameAs: job.company.website || null,
			logo: job.company.logo,
		},
	}
}
