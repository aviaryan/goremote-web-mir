import { useRouter } from 'next/router'
import { useState, useRef } from 'react'
import SearchJob from './SearchJob'
import JobListing from './JobListing'
import JobOverlay from './JobOverlay'
import { logJobOverlayOpen } from '../utils/analytics'
import MetaForList from './MetaForList'
import { CancelTag } from './Tag'
import InListAd from './InListAd'

const JobListBlock = ({ children }) => <div className='py-2 block cursor-default'>{children}</div>

const Ad = () => (
	<JobListBlock key='ad'>
		<InListAd />
	</JobListBlock>
)

export default function JobList({ category = null, jobs = [], tags = [] }) {
	const router = useRouter()
	const [openedJob, setOpenedJob] = useState(null)
	const oldRoute = useRef(router.asPath)

	function jobClick(jobData) {
		if (window.innerWidth > 768) {
			openJobOverlay(jobData)
		} else {
			window.location.href = `/job/${jobData.urlSuffix}`
		}
	}

	function openJobOverlay(jobData) {
		if (window.innerWidth < 768) {
			return
		}
		// don't use router.asPath as it contains server side fixed route data
		oldRoute.current = window.location.pathname + window.location.search //router.asPath
		// console.log(router)
		// not using router.push since it fails with [dynamic] rootSelector router
		// and we don't need that, we just need a fake change
		// https://err.sh/zeit/next.js/incompatible-href-as
		window.history.replaceState(null, null, `/job/${jobData.urlSuffix}`)
		setOpenedJob(jobData)
		logJobOverlayOpen(jobData.urlSuffix)
	}

	function hideOverlay() {
		window.history.replaceState(null, null, oldRoute.current)
		setOpenedJob(null)
	}

	const jobsDOM = jobs.map((data, ind) => (
		<JobListBlock key={ind}>
			<JobListing data={data} onClick={() => jobClick(data)} includeSchema={ind < 50} />
		</JobListBlock>
	))

	const promotedJobsCount = jobs.reduce((sum, cur) => sum + (cur.promScore > 0 ? 1 : 0), 0)
	const showAd = jobsDOM.length > 3
	// using 0.5 instead of random because HTML rendering inconsistency
	const adPos =
		promotedJobsCount + Math.ceil((0.5 * Math.min(jobsDOM.length - promotedJobsCount, 30)) / 2)

	const heading = `Remote ${category || ''} Jobs in the Indian Timezone`

	return (
		<div className='container mx-auto'>
			<MetaForList jobs={jobs} />
			{openedJob && <JobOverlay onDismiss={hideOverlay} data={openedJob} />}
			<h1 className='text-center text-3xl'>{heading}</h1>
			<div className='flex justify-center py-8'>
				{category ? <CancelTag tagName={category} /> : <SearchJob tags={tags} />}
			</div>
			<div className='py-4 max-w-4xl mx-auto'>
				{jobsDOM.slice(0, adPos)}
				{showAd && <Ad />}
				{jobsDOM.slice(adPos)}
			</div>
		</div>
	)
}
