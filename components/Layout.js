import Head from 'next/head'
import Header from './Header'
import Footer from './Footer'
import ByTwitter from './ByTwitter'
import EmailFloater from './EmailFloater'

const desc = `GoRemote is the #1 site for remote jobs in the Indian timezone.
We have 400+ listing of fully remote jobs, from development to data science to designing,
suitable for anyone in the Indian timezone and around.`.replace(/\n/g, ' ')

const Layout = props => (
	<div className='w-screen min-h-screen flex flex-col'>
		<Head>
			<title key='title'>Remote Jobs in the Indian Timezone &middot; GoRemote</title>

			<link rel='shortcut icon' href='/favicon.ico' />
			<link rel='icon' sizes='16x16 32x32' href='/favicon.ico' />
			<link
				rel='icon'
				type='image/png'
				sizes='16x16 32x32 64x64 128x128 256x256'
				href='/favicon.png'
			/>
			<link rel='apple-touch-icon' href='/favicon.png' />

			<link key='sitemap' rel='sitemap' href='/sitemap.xml' />

			<meta name='viewport' content='initial-scale=1.0, width=device-width' key='viewport' />
			<meta key='description' name='description' content={desc} />
		</Head>
		<Header />
		<div className='container mx-auto py-8 pb-16 flex-grow px-2 md:px-4'>{props.children}</div>
		<Footer />
		<EmailFloater />
		<ByTwitter />
	</div>
)

export default Layout
