import { useState, useMemo } from 'react'
import Autosuggest from 'react-autosuggest'
import { makeCategoryCaption } from '../lib/strings'
import { getUrlFromTag } from '../lib/jobs'

function makeSuggestionItemFromTag(tag) {
	return {
		label: makeCategoryCaption(tag),
		value: tag,
	}
}

const getSuggestions = (value, tagsSuggestions) => {
	const inputValue = value.trim().toLowerCase()
	if (!inputValue) {
		return []
	}
	return tagsSuggestions.filter(tag => tag.label.toLowerCase().indexOf(inputValue) > -1)
}

const getSuggestionValue = suggestion => suggestion.label

const renderSuggestion = suggestion => <span>{suggestion.label}</span>

const theme = {
	container: 'relative w-full lg:w-1/2 text-xl',
	input: 'w-full border border-dashed border-gray-600 px-2',
	suggestionsContainerOpen: 'absolute w-full bg-white shadow z-10',
	suggestion: 'cursor-pointer px-2 py-2 border-b',
	suggestionHighlighted: 'bg-blue-200',
}

function onSuggestionSelected(_, { suggestion }) {
	if (suggestion) {
		window.location.href = getUrlFromTag(suggestion.value)
	}
}

export default function SearchJob({ tags = [] }) {
	const [value, setValue] = useState('')
	const [suggestions, setSuggestions] = useState([])
	const tagsSuggestions = useMemo(() => tags.map(makeSuggestionItemFromTag), [tags])
	// console.log(tagsSuggestions)

	const onChange = (_, { newValue }) => {
		setValue(newValue)
	}

	const onSuggestionsFetchRequested = ({ value }) =>
		setSuggestions(getSuggestions(value, tagsSuggestions))

	const onSuggestionsClearRequested = () => setSuggestions([])

	const inputProps = {
		placeholder: 'React, Python, ...',
		value,
		onChange: onChange,
	}

	return (
		<Autosuggest
			suggestions={suggestions}
			onSuggestionsFetchRequested={onSuggestionsFetchRequested}
			onSuggestionsClearRequested={onSuggestionsClearRequested}
			onSuggestionSelected={onSuggestionSelected}
			getSuggestionValue={getSuggestionValue}
			renderSuggestion={renderSuggestion}
			inputProps={inputProps}
			theme={theme}
		/>
	)
}
