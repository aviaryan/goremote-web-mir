import { stopPropagation } from '../lib/dom'
import { getUrlFromTag } from '../lib/jobs'

const tagClasses = 'p-1 bg-gray-400 text-black rounded text-sm truncate shadow cursor-pointer'

export default function Tag({ tagName, className = '' }) {
	const onTagClick = e => {
		stopPropagation(e)
	}

	return (
		<a
			className={tagClasses + ' hover:bg-gray-900 hover:text-white ' + className}
			onClick={onTagClick}
			href={getUrlFromTag(tagName)}
		>
			{tagName}
		</a>
	)
}

export function CancelTag({ tagName }) {
	return (
		<a href='/' className={tagClasses}>
			<b className='pr-2'>X</b>
			{tagName}
		</a>
	)
}
