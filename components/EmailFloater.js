import React, { useState, useEffect } from 'react'

const key = 'hide-fixed-email'

export default function EmailFloater() {
	// start with true so that no blink if not showing
	const [isHiding, setHiding] = useState(true)

	useEffect(() => {
		let status = localStorage.getItem(key)
		// if nothing set, show collector
		if (!status) {
			setHiding(false)
			return
		}
		// else check datetime value
		const curTimeMillis = new Date().getTime()
		// const sevenDays = 1000 * 60 * 60 * 24 * 7
		const fiveMinutes = 1000 * 60 * 5
		status = Number(status)
		// if more than X time have passed since hidden, show it back
		if (curTimeMillis - status > fiveMinutes) {
			setHiding(false)
			localStorage.setItem(key, '')
		}
		// else do nothing
	}, [])

	function hide() {
		setHiding(true)
		const curTimeMillis = new Date().getTime()
		localStorage.setItem(key, curTimeMillis)
	}

	if (isHiding) {
		return null
	}

	return (
		<div
			className={
				'bg-black w-full fixed bottom-0 z-10 text-white text-sm flex ' +
				'items-center justify-center py-2'
			}
		>
			<div className='py-2 w-full text-center px-2'>
				<span>
					Join our email list to get new job listings and free educational content every week
				</span>
				<a
					className={
						'ml-2 text-white px-2 py-1 rounded shadow cursor-pointer ' +
						'bg-black border-white border hover:bg-white hover:text-black'
					}
					href='https://emailoctopus.com/lists/10782f55-39cd-11ea-be00-06b4694bee2a/forms/subscribe'
					target='_blank'
				>
					Subscribe
				</a>
			</div>
			<span className='ml-auto mr-2 cursor-pointer text-lg text-white font-bold' onClick={hide}>
				X
			</span>
		</div>
	)
}
