export default function InListAd() {
	return (
		<a className={'p-4 flex items-center bg-green-400 cursor-pointer'} href='/free-email-course'>
			<div>
				<img
					data-src={'/favicon.png'}
					alt={`GoRemote logo`}
					className='lazyload h-20 w-20 md:w-24 md:h-24 max-w-unset'
				/>
			</div>

			<div className='px-4 overflow-auto'>
				<h2 className='text-xl'>
					<b>New to remote work?</b>
				</h2>
				<p className='pt-2 break-words'>
					Join our free email course on getting a remote job in India.
				</p>
			</div>
		</a>
	)
}
