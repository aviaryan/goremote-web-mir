import Head from 'next/head'
import { stripHTML } from '../lib/strings'

const socialImage =
	'https://res.cloudinary.com/aviaryan/image/upload/v1579592931/goremote/social_image_yrglmm.png'

export default function JobHead({ data }) {
	const title = `${data.title} at ${data.company.name} · Jobs at GoRemote`
	const excerpt = stripHTML(data.description).substring(0, 250)

	return (
		<Head>
			<title key='title'>{title}</title>
			<meta key='description' name='description' content={excerpt} />

			<meta key='og:title' property='og:title' content={title} />
			<meta key='og:type' property='og:type' content='website' />
			<meta key='og:url' property='og:url' content={'https://goremote.in/job/' + data.urlSuffix} />
			<meta key='og:image' property='og:image' content={socialImage} />
			<meta key='og:description' property='og:description' content={excerpt} />
			<meta key='og:site_name' property='og:site_name' content='GoRemote' />

			<meta key='twitter:card' name='twitter:card' content='summary_large_image' />
			<meta key='twitter:site' name='twitter:site' content='@GoRemoteIn' />
			<meta key='twitter:title' name='twitter:title' content={title} />
			<meta key='twitter:description' name='twitter:description' content={excerpt} />
			<meta key='twitter:creator' name='twitter:creator' content='@GoRemoteIn' />
			<meta key='twitter:image:src' name='twitter:image:src' content={socialImage} />
		</Head>
	)
}
