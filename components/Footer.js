export default function Footer() {
	return (
		<footer className='bg-gray-400 py-12 mt-8 flex flex-col justify-center items-center px-4'>
			<div className='py-4'>
				Don't miss out on new job listings, product updates, educational content and more.
			</div>
			<a
				className='text-white px-3 py-2 rounded shadow cursor-pointer bg-blue-800'
				href='https://emailoctopus.com/lists/10782f55-39cd-11ea-be00-06b4694bee2a/forms/subscribe'
				target='_blank'
			>
				Subscribe to our email list
			</a>
		</footer>
	)
}
