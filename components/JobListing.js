import Tag from './Tag'
import { getJobPostedSmallString, getHumanDate } from '../lib/date'
import JobSchema from './JobSchema'

export default function JobListing({ data, onClick, includeSchema = false }) {
	const tagsDOM = (data.tags || '')
		.split('|')
		.filter(tag => tag)
		.map(tag => <Tag tagName={tag} key={tag} className={'mx-1 leading-loose'} />)

	return (
		<div
			className={'p-4 flex items-center ' + (data.promScore === 5 ? 'bg-yellow-400' : 'bg-red-400')}
			onClick={onClick}
		>
			{includeSchema && <JobSchema job={data} />}
			<a itemProp='url' href={`/job/${data.urlSuffix}`}></a>
			<div>
				<img
					data-src={data.company.logo}
					alt={`${data.company.name} logo`}
					className='lazyload h-20 w-20 md:w-24 md:h-24 max-w-unset'
				/>
			</div>
			<div className='px-4 overflow-auto'>
				<h2 className='text-xl'>
					<b>{data.title}</b> at{' '}
					<span itemProp='hiringOrganization' itemScope itemType='http://schema.org/Organization'>
						<span itemProp='name'>{data.company.name}</span>
					</span>
				</h2>
				<p className='pt-2 break-words'>
					<span className='pr-2' title={getHumanDate(data.postedAt)}>
						{getJobPostedSmallString(data.postedAt)}
					</span>
					{tagsDOM}
				</p>
			</div>
		</div>
	)
}

// itemprop from JobPosting schema example 4
// https://schema.org/JobPosting
