import React from 'react'
import JobDetail from './JobDetail'
import { stopPropagation } from '../lib/dom'
import JobHead from './JobHead'

export default function JobOverlay({ data, onDismiss }) {
	return (
		<div className='fixed top-0 left-0 w-full h-full z-50 bg-smoke flex' onClick={onDismiss}>
			<JobHead data={data} />
			<div
				className='relative p-12 bg-white w-full max-w-5xl m-auto overflow-y-scroll max-h-90'
				onClick={stopPropagation}
			>
				<JobDetail data={data} />
			</div>
		</div>
	)
}
