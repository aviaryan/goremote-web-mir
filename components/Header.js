import Link from 'next/link'

export default function Header() {
	return (
		<header className='text-base md:text-lg bg-gray-200 py-2 px-1'>
			<div className='container mx-auto flex justify-between'>
				<div>
					<span className='cursor-pointer'>
						<a href='/'>GoRemote</a>
					</span>
				</div>
				<div className='text-base'>
					{/* <span className='px-1 cursor-pointer'>
						<a href='/free-email-course'>Free Remote Job Course</a>
					</span>
					<span className='px-1 text-gray-500'>|</span> */}
					<span className='px-1 cursor-pointer'>
						<Link href='/post-a-job'>
							<a>Post a Job</a>
						</Link>
					</span>
				</div>
			</div>
		</header>
	)
}
