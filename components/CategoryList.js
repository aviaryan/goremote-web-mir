import Head from 'next/head'
import JobList from './JobList'
import Layout from './Layout'
import { makeCategoryCaption } from '../lib/strings'

export default function CategoryList({ category, jobs }) {
	const properCategory = makeCategoryCaption(category)
	return (
		<Layout>
			<Head>
				<title key='title'>{`Remote ${properCategory} Jobs for Indians · GoRemote`}</title>
			</Head>
			<JobList category={properCategory} jobs={jobs} />
		</Layout>
	)
}
