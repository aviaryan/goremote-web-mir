# Frontend for GoRemote

Uses [NextJS](https://nextjs.org/docs/getting-started). Port is 3006.

### Development

```
yarn dev
```

### Prod

https://nextjs.org/learn/basics/deploying-a-nextjs-app/

```
yarn build
yarn start
```

### Testing Prod Locally

Do the same thing above. Since `PROD` is not set locally, it will work.
