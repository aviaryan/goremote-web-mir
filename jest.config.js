module.exports = {
	testPathIgnorePatterns: ['<rootDir>/.next/', '<rootDir>/node_modules/'],
	transform: {
		'^.+\\.js$': 'babel-jest',
	},
	moduleNameMapper: {
		'\\.(css|less)$': 'identity-obj-proxy',
	},
}
