export function preventEventDefault(e) {
	e.preventDefault()
	return false
}

export function stopPropagation(e) {
	e.stopPropagation()
}
