/**
 * gets category page URL from tag preceeding with (/) slash
 * @export
 * @param {string} tag
 * @return url with / prefix
 */
export function getUrlFromTag(tag) {
	return `/jobs/${tag.toLowerCase()}-jobs`
}

export function attachRefToUrl(applyURL) {
	if (applyURL.indexOf('?') > -1) {
		applyURL += '&'
	} else {
		applyURL += '?'
	}
	applyURL += 'utm_source=goremotein&ref=goremotein'
	return applyURL
}
