export function addDaysToDate(date, days = 0) {
	const result = new Date(date)
	result.setDate(result.getDate() + days)
	return result
}

function getSecondsDiffFromNow(dateString) {
	const dt = new Date(dateString)
	const curDate = new Date()
	const diff = (curDate - dt) / 1000
	return diff
}

export function getJobPostedSmallString(dateString) {
	const diff = getSecondsDiffFromNow(dateString)
	if (diff < 3600) {
		return 'now'
	} else if (diff < 3600 * 24) {
		const hrs = Math.floor(diff / 3600)
		return hrs + 'h'
	} else {
		const days = Math.floor(diff / (3600 * 24))
		return days + 'd'
	}
	// not keeping mins because confusion with months
}

export function getJobPostedLargeString(dateString) {
	const diff = getSecondsDiffFromNow(dateString)
	if (diff < 60) {
		return 'Posted recently'
	} else if (diff < 3600) {
		const mins = Math.floor(diff / 60)
		return 'Posted ' + mins + ' minutes ago'
	} else if (diff < 3600 * 24) {
		const hrs = Math.floor(diff / 3600)
		return 'Posted ' + hrs + ' hours ago'
	} else {
		const days = Math.floor(diff / (3600 * 24))
		return 'Posted ' + days + ' days ago'
	}
}

export function getHumanDate(dateString) {
	return new Date(dateString).toString()
}
