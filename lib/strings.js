export function titleCase(str) {
	str = str.toLowerCase().split('-')
	for (var i = 0; i < str.length; i++) {
		str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1)
	}
	return str.join(' ')
}

export function makeCategoryCaption(category) {
	category = titleCase(category)
	return category
		.replace(/([^\.]|^)js/gi, '$1JS')
		.replace(/\bios\b/gi, 'iOS')
		.replace(/\.net/gi, '.NET')
		.replace(/sql/gi, 'SQL')
		.replace(/php/gi, 'PHP')
		.replace(/html/gi, 'HTML')
		.replace(/css/gi, 'CSS')
		.replace(/([^\.])db/g, '$1DB')
		.replace(/ql/gi, 'QL')
		.replace(/devops/gi, 'DevOps')
}

export function stripHTML(html) {
	return html.replace(/<[^>]*>?/gm, '').replace(/\s+/g, ' ')
}
