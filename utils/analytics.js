import ReactGA from 'react-ga'

export const initGA = () => {
	// console.log('GA init')
	ReactGA.initialize('UA-42460130-8')
}

export const logPageView = () => {
	// console.log(`Logging pageview for ${window.location.pathname}`)
	ReactGA.set({ page: window.location.pathname })
	ReactGA.pageview(window.location.pathname)
}

const logEvent = (category = '', action = '', label = '') => {
	if (category && action) {
		ReactGA.event({ category, action, label })
	}
}

export const logException = (description = '', fatal = false) => {
	if (description) {
		ReactGA.exception({ description, fatal })
	}
}

// loggers

// export function logSearch(value) {
// 	logEvent('Interaction', 'Search', value)
// }

export function logJobOverlayOpen(jobPrefixURL) {
	logEvent('Interaction', 'JobOverlayOpen', jobPrefixURL)
}

export function logJobApply(value) {
	logEvent('Outbound', 'Apply', value)
}
