const withCSS = require('@zeit/next-css')

module.exports = withCSS({
	env: {
		// only set PROD in actual PROD environment
		PROD: process.env.PROD ? true : false,
	},
})
