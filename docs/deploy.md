## Deployed to Dokku

Taken from **smc-backend**

### Initial Steps - Setup server

- ssh keygen create a ssh
- attach it

```sh
chmod 400 private
ssh -i smc-htz root@159.69.91.47
```

- Update the server

```sh
sudo apt-get upgrade
sudo apt-get update
```

### Dokku

- install dokku from ssh
- go to web url of server
- fix the public key, check the IP and then leave it. The public key is the one used to ssh to server.

#### install app

```sh
dokku apps:create grmw
# set config
dokku config:set grmw PROD=true
# without following, building is not minified
dokku config:set grmw NODE_ENV=production
```

### set DNS to Cloudflare

Change nameservers for Cloudflare. Wait till active. Then add the new server IP.

![cloudflare DNS](https://user-images.githubusercontent.com/4047597/71988868-aaa90000-3256-11ea-87d0-4fbb7420b47f.png)

### ports

Need to set it to 80 to serve it on HTTP.

```sh
# port (also open in inbound security)
dokku proxy:ports-set grmw http:80:3006
# for https see below
```

![ports](https://user-images.githubusercontent.com/4047597/71983836-a9bfa080-324d-11ea-9131-0501039cc1dd.png)

## attach domain

```sh
dokku domains:set grmw goremote.in
```

## push app

```sh
git remote add dokku dokku@159.69.91.47:grmw
git push -v dokku master
```

## https

add A record to IP. Keep it full - DNS and HTTP

```sh
dokku certs:generate grmw goremote.in
# dokku proxy:ports-add grmw https:443:3006
# https (also open), no private key, generates a CSR
# sign CSR by cloudflare
# and that should be done, if cloudflare is forwarding to https, it will recognize its CSR and forward it under its name too
# otherwise use certs:add to add the key - cert pair from cloudflare
```

![setting https](https://user-images.githubusercontent.com/4047597/71984468-e770f900-324e-11ea-8dc7-720353864e05.png)

![cloudflare https](https://user-images.githubusercontent.com/4047597/71985709-16886a00-3251-11ea-95ac-32d457d7dad1.png)

### Restarting and stopping app

```sh
# rebuilds image
dokku ps:rebuild grmw
# start
dokku ps:start grmw
# stop
dokku ps:stop grmw
```

### scaling

```sh
dokku ps:scale grmw web=1 cron=1
```

### View logs

```sh
dokku logs grmw
# for process
dokku logs grmw -p cron
```

### About the 80 port on host

Seems like the 80 port is hosting multiple apps. If you go to 80 port directly, then it shows the most recent deployed app. In the following example, `grmw` is shown even though jarvis and aavi.me is to be shown on port 80.

But nginx checks domain before returning data on port 80 so if you go to `goremote.in` or `jarvis.aviaryan.com`, it can return the correct thing.

![port 80 of IP](https://user-images.githubusercontent.com/4047597/71986758-007ba900-3253-11ea-8ee9-e1828431af13.png)

### Inspect or shell into image

Inspect image if container not running

```sh
docker run -it ff6296d9f276 /bin/sh
```

If running, use enter.

```sh
dokku enter grmw
```

### internal backend connection (way 1)

this shares the host's network stack with grmw.

```sh
dokku docker-options:add grmw deploy "--network=host"
# to check
dokku docker-options:report grmw
```

This way **fails** to show `grmw` on port 80 and 443.

### internal network (way 2)

https://forums.docker.com/t/how-to-reach-a-container-from-another-container-without-ip-of-dockernat/21083/3

```sh
docker network create grm
docker network list
dokku docker-options:add grmw deploy "--net grm"
# access
dokku enter grmw
curl http://grmb.web.1:3004
```

### Gotcha: Building before serving

Since NextJS needs to build before serve, that was an issue. Can't say if `app.json -> predeploy` is working so added `postinstall` in `package.json > scripts` to fix it. `Procfile` should not have building step.
Added `$PROD` variable check for `postinstall`.
