# EC2 Deploy

This file contains extra steps to set up a Dokku server on EC2.

### Starting the server

1. Go to Mumbai region and create EC2 instance. Ubuntu x64.
2. Choose t2.micro free tier type.
3. Choose 8GB Storage (default). It's EBS (/home/ubuntu) so it will persist. Remove Delete on Termination option. https://stackoverflow.com/questions/24551649/
4. Allow backend IP and http(s) IP in public.
5. Launch it. Got the `ec2-india-goremote` key pair.
6. Create elastic IP and associate with it. In this case, I got 15.206.51.244.
7. Attach EBS to it. It looks like root volume is already attached. See https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-using-volumes.html. It persisted on reboot.

![Screenshot 2020-01-17 at 8 01 54 PM](https://user-images.githubusercontent.com/4047597/72619865-3a8c2f80-3964-11ea-9330-a6038d78c485.png)

### Installing Dokku

1. Install with wget bootstrap.sh simply.
2. Visit the elastic IP in browser and make sure ssh key is set properly. Also change hostname to IP (15.206.51.244).

Got the public SSH key from browser. Allows to access `dokku` user via ssh. `ssh -i that-key dokku@IP <dokku command>`. Runs that dokku command from my system.

### Setting up app

**Important** - Set apps one by one. First create one app. Then deploy it. The set its ports. Then set its options.
Then second app. Ports. Options. Then docker network. Then adding the network.

Follow same steps as deploy.md

Issue with docker in sudo though (since dokku installed as sudo). Need to give user `ubuntu` access to docker.

```
sudo usermod -a -G docker ubuntu
```

Then restart user (can do ec2 reboot). Then `docker network create` will work.

Or can just run all docker commands with sudo.

```sh
sudo docker network ...
```

### Remote

```sh
git remote add dokku2 dokku@15.206.51.244:grmw
```

### Important note on restarting app

New Dokku doesn't restart apps on options change. So need to restart it everytime.

```sh
dokku ps:restart <app>
```

### Nginx starting/restarting

```sh
sudo service nginx start  # stop restart
```

### Networking

Docker network is broken in dokku v0.19.12 so I am using network host for now. Can access backend on 127.0.0.1:51234.

```sh
dokku docker-options:add grmw deploy "--network host"
```

Need to disable checks as network host means port is on 3006 which means old port will conflict with new deploying port.

```sh
dokku checks:disable grmw web
```

### Issue with performance

Issue with resource being 100% consumed on my small server. Only 1GB Ram and 8GB Disk.
Need to increase disk size from 8GB to 16GB, maybe RAM as well.
Increased volume disk size and then restart EC2 server to load it. Check using `df -h`. Seems to help.

### Logging

Using papertrail and following [this guide](http://mikebian.co/sending-dokku-container-logs-to-papertrail/).

```sh
dokku plugin:install https://github.com/michaelshobbs/dokku-logspout.git
dokku logspout:server syslog+tls://logs.papertrailapp.com:48511
dokku plugin:install https://github.com/michaelshobbs/dokku-hostname.git dokku-hostname
dokku logspout:start
# to view logs
dokku logspout:stream
# first time to fix hostnames
dokku ps:rebuild grmb
dokku ps:rebuild grmw
# also stop and start logspout if needed
```

It seems it uses single system for all apps on a server. Need to use saved searches feature. Uses new system for a new server (even when destination is same).
