### Software functional requirements

- Website to search remote jobs in India
- Filters for salary, company timezone, etc.
- Strong search in description if possible.
- Newsletter
- Articles and Interviews
- Pages for selling courses and shit
- Ability for companies to submit job posts, show it instantly or after manual validation.

### Non-functional requirements

- Might need a backend for search if dataset is large. If Gatsby can handle that page splitting for API, that's the best.
