# Monorepo

Why monorepo? So that common classes can be easily used. But I realize if we don't have TypeScript, it doesn't make a lot of sense.

### Gotchas

`.babelrc` for NextJS causes issues with the babelrc needed for Express + ES6.
So we have excluded `.babelrc` for building `cron` and rather specify the preset on the command line.
Tests with Jest cause issue too that's why not specifying any `.babelrc` in the `cron/` folder.

```json
{
	"cron:build": "babel --no-babelrc cron/ --out-dir cron/dist/ --ignore 'dist/*' --presets=es2015",
	"cron:start": "yarn cron:build && node cron/dist/cron.js",
	"cron:dev": "nodemon --watch 'cron/**/*.js' --ignore 'dist/*' --exec 'yarn' cron:start"
}
```

### Issues with monorepo

In building and compiling files. Had to remove it.

https://github.com/aviaryan/goremote-web/releases/tag/v0.1.1
