import Layout from '../../components/Layout'
import JobDetail from '../../components/JobDetail'
import JobHead from '../../components/JobHead'
import OrgSchema from '../../components/OrgSchema'
import JobSchema from '../../components/JobSchema'
import { getJobById } from '../../data/api'
import Error from '../_error'

const Job = ({ job }) => {
	if (!job) {
		return <Error status={404} />
	}
	return (
		<Layout>
			<JobHead data={job} />
			<div className='w-full py-8 mx-auto max-w-4xl'>
				<OrgSchema key={`orgJSON-${job.company.name}`} company={job.company} />
				<OrgSchema key={`orgJSON-GoRemote`} company={null} />
				<JobSchema job={job} />
				<JobDetail data={job} />
			</div>
		</Layout>
	)
}

Job.getInitialProps = async ({ query }) => {
	if (typeof window !== 'undefined') {
		return location.reload()
	}
	const jobID = query.jobID
	const realID = jobID.match(/\d+/)[0]
	const job = await getJobById(realID)
	return { job }
}

export default Job
