import Layout from '../components/Layout'

export default function PostJob() {
	return (
		<Layout>
			<div className='max-w-4xl mx-auto'>
				<h1 className='text-3xl'>Post a Job</h1>
				<span>
					Please contact me at <a href='mailto:avi@aviaryan.com'>avi(at)aviaryan.com</a> with the
					job details.
				</span>
			</div>
		</Layout>
	)
}
