import Error from '../_error'
import CategoryList from '../../components/CategoryList'
import { getJobsBySearch } from '../../data/api'

export default function Matcher({ jobs, category }) {
	if (category) {
		return <CategoryList jobs={jobs} category={category} />
	}

	return <Error status={404} />
}

function getPageTypeWithAttributes({ query }) {
	const selector = query.matcher

	if (!selector) {
		return null
	}

	if (selector.endsWith('-jobs')) {
		const category = selector.replace('-jobs', '')
		if (category.match(/^[a-z0-9\-\.]+$/i)) {
			return { category }
		}
	}

	return null
}

Matcher.getInitialProps = async ctx => {
	if (typeof window !== 'undefined') {
		return location.reload()
	}
	const pageAttrs = getPageTypeWithAttributes(ctx)
	if (!pageAttrs) {
		return { error: true }
	}
	if (pageAttrs.category) {
		const jobs = await getJobsBySearch(pageAttrs.category)
		return {
			jobs,
			// ...pageAttrs,
			category: pageAttrs.category,
		}
	}
}
