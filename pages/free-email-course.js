const url = 'https://emailoctopus.com/lists/bb530531-39cb-11ea-be00-06b4694bee2a/forms/subscribe'

const Comp = () => {}

Comp.getInitialProps = async ({ res }) => {
	res.writeHead(302, { Location: url })
	res.end()
	return {}
}

export default Comp
