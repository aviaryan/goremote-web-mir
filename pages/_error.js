import Layout from '../components/Layout'

function Error({ statusCode }) {
	return (
		<Layout>
			<div className='flex items-center justify-center'>
				<p>
					{statusCode ? `An error ${statusCode} occurred on server` : 'This page does not exist'}
				</p>
			</div>
		</Layout>
	)
}

Error.getInitialProps = ({ res, err }) => {
	const statusCode = res ? res.statusCode : err ? err.statusCode : 404
	return { statusCode }
}

export default Error
