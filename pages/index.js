import Layout from '../components/Layout'
import JobList from '../components/JobList'
import { getHomePageJobs, getTags } from '../data/api'

export default function HomePage({ jobs, tags }) {
	return (
		<Layout>
			<JobList jobs={jobs} tags={tags} />
		</Layout>
	)
}

HomePage.getInitialProps = async () => {
	if (typeof window !== 'undefined') {
		return location.reload()
	}
	const jobs = await getHomePageJobs()
	const tags = await getTags()
	// console.log(jobs)
	return { jobs, tags }
}
