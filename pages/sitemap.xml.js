import { getSitemap } from '../data/api'

const Sitemap = () => {}

Sitemap.getInitialProps = async ({ res }) => {
	const sitemapRes = await getSitemap()
	const text = await sitemapRes.text()

	res.setHeader('content-type', 'application/xml')
	res.end(text)

	return {}
}

export default Sitemap
