// purgecss taken from https://tailwindcss.com/docs/controlling-file-size/
const purgecss = require('@fullhuman/postcss-purgecss')({
	content: ['./pages/**/*.js', './components/**/*.js'],
	defaultExtractor: content => content.match(/[\w-/:]+(?<!:)/g) || [],
})

const cssnano = require('cssnano')({
	preset: 'default',
})

module.exports = {
	plugins: [
		require('postcss-import'),
		require('tailwindcss')('./css/tailwind.config.js'),
		require('autoprefixer'),
		...(process.env.PROD ? [purgecss, cssnano] : []),
	],
}
