const softwareDevData = {
	title: 'Software Engineer',
	tags: '|github|go|react|',
	company: {
		city: 'San Francisco',
		name: 'Google',
		logo: 'https://aviaryan.com/favicon.png',
	},
	description: 'Description',
	applyURL: 'https://stackoverlf.com/jobs/221',
}

export { softwareDevData }
