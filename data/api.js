import fetch from 'isomorphic-unfetch'

// automatically resolves on server
const rootURL = process.env.PROD ? 'http://localhost:51234/' : 'http://127.0.0.1:3004/'

export async function getHomePageJobs() {
	const res = await fetch(rootURL + 'jobs')
	const data = await res.json()
	return data
}

export async function getTags() {
	const res = await fetch(rootURL + 'jobs/tags')
	const data = await res.json()
	return data
}

export async function getJobsBySearch(searchTerm) {
	const res = await fetch(rootURL + 'jobs/s/' + searchTerm)
	const data = await res.json()
	return data
}

export async function getJobById(jobID) {
	const res = await fetch(rootURL + 'jobs/' + jobID)
	const data = await res.json()
	return data
}

export async function getSitemap() {
	const res = await fetch(rootURL + 'sitemap.xml')
	return res
}
