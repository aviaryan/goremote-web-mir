import { LoremIpsum } from 'lorem-ipsum'

const lorem = new LoremIpsum({
	sentencesPerParagraph: {
		max: 8,
		min: 4,
	},
	wordsPerSentence: {
		max: 16,
		min: 4,
	},
})

function generateJobCollection(role) {
	const ls = [1, 2, 3, 4, 5]
	return ls.map(num => ({
		company: {
			name: lorem.generateWords(1),
			logo: 'https://aviaryan.com/favicon.png',
			country: 'Australia',
		},
		title: role,
		description: lorem.generateParagraphs(6).replace(/\./g, '.<br/>'),
		id: num * (role === 'React Dev' ? 1 : 2),
		applyURL: 'https://stackoverflow.com/jobs/341983/',
	}))
}

const reactJobs = generateJobCollection('React Dev')
const pythonJobs = generateJobCollection('Python Guru')

function getJobByURL(url) {
	const jobLists = [reactJobs, pythonJobs]
	for (const ind in jobLists) {
		const matchedJob = jobLists[ind].filter(job => job.url === url)[0]
		if (matchedJob) {
			return matchedJob
		}
	}
	return null
}

export { reactJobs, pythonJobs, getJobByURL }
