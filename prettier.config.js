module.exports = {
	endOfLine: 'lf',
	semi: false,
	singleQuote: true,
	useTabs: true,
	tabWidth: 2,
	jsxSingleQuote: true,
	printWidth: 100,
	bracketSpacing: true,
	trailingComma: 'es5',
	// .travis.yml doesn't work with tabs
	overrides: [
		{
			files: '*.yml',
			options: {
				useTabs: false,
			},
		},
	],
}
