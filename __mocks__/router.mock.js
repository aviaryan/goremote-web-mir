import { RouterContext } from 'next/dist/next-server/lib/router-context'

export function withMockedRouter(Component, props) {
	return (route, pathname = null, query = {}) => {
		const router = {
			pathname: pathname || route,
			route,
			query,
			asPath: pathname || route,
		}

		return (
			<RouterContext.Provider value={router}>
				<Component {...props} />
			</RouterContext.Provider>
		)
	}
}
