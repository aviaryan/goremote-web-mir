import { render } from '@testing-library/react'
import Matcher from '../../pages/jobs/[matcher]'
import { withMockedRouter } from '../../__mocks__/router.mock'
import { reactJobs, pythonJobs } from '../../data/data'

describe('Testing Jobs Pages', () => {
	it('React Jobs Page Loads', () => {
		const { getByText, getAllByText } = render(
			withMockedRouter(Matcher, { category: 'react', jobs: reactJobs })(
				'/jobs/[matcher]',
				'/react-jobs',
				{
					matcher: 'react-jobs',
				}
			)
		)
		expect(getByText('Remote React Jobs', { exact: false })).not.toBeNull()
		expect(getAllByText('React Dev', { exact: false })).not.toBeNull()
	})

	it('Python Jobs Page Loads', () => {
		const { getByText, getAllByText } = render(
			withMockedRouter(Matcher, { category: 'python', jobs: pythonJobs })(
				'/jobs/[matcher]',
				'/python-jobs',
				{
					matcher: 'python-jobs',
				}
			)
		)
		expect(getByText('Remote Python Jobs', { exact: false })).not.toBeNull()
		expect(getAllByText('Python Guru', { exact: false })).not.toBeNull()
	})
})
