import { render } from '@testing-library/react'
import Index from './../../pages/index'
import { withMockedRouter } from '../../__mocks__/router.mock'
import { reactJobs } from '../../data/data'

describe('Testing HomePage', () => {
	it('HomePage Loads Correctly', () => {
		const { getByText, getAllByText } = render(withMockedRouter(Index, { jobs: reactJobs })('/'))
		expect(getByText('Remote Jobs', { exact: false })).not.toBeNull()
		expect(getAllByText('React Dev', { exact: false })).not.toBeNull()
	})
})
