import { titleCase, makeCategoryCaption } from '../../lib/strings'

test('titleCase function works as expected', () => {
	expect(titleCase('data-entry')).toBe('Data Entry')
	expect(titleCase('marketing')).toBe('Marketing')
})

test('makeCategoryCaption function works as expected', () => {
	expect(makeCategoryCaption('reactjs')).toBe('ReactJS')
	expect(makeCategoryCaption('js')).toBe('JS')
	expect(makeCategoryCaption('node.js')).toBe('Node.js')
	expect(makeCategoryCaption('project-management')).toBe('Project Management')
	expect(makeCategoryCaption('mongodb')).toBe('MongoDB')
	expect(makeCategoryCaption('sql')).toBe('SQL')
	expect(makeCategoryCaption('Postgresql')).toBe('PostgreSQL')
	expect(makeCategoryCaption('php')).toBe('PHP')
	expect(makeCategoryCaption('Html')).toBe('HTML')
	expect(makeCategoryCaption('Css')).toBe('CSS')
	expect(makeCategoryCaption('graPhqL')).toBe('GraphQL')
	expect(makeCategoryCaption('devoPS')).toBe('DevOps')
	expect(makeCategoryCaption('Ios')).toBe('iOS')
})
