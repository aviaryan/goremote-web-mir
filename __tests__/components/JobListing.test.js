import React from 'react'
import { render } from '@testing-library/react'
import JobListing from './../../components/JobListing'
import { softwareDevData } from '../../data/testData'

describe('Testing JobListing', () => {
	it('Loads Data Without Error', () => {
		const { getByText, queryByText } = render(<JobListing data={softwareDevData} />)

		expect(getByText('Software Engineer', { selector: 'b' })).not.toBeNull()
		expect(getByText('Google', { exact: false })).not.toBeNull()

		// don't show description in JobListing
		expect(queryByText('Description')).toBeNull()
		// no value that needs to be shown is missing
		expect(queryByText('undefined', { exact: 'false' })).toBeNull()
	})
})
