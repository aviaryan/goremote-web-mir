import React from 'react'
import { render } from '@testing-library/react'
import JobDetail from './../../components/JobDetail'
import { softwareDevData } from '../../data/testData'

describe('Testing JobDetail', () => {
	it('Loads Data Without Error', () => {
		const { getByText, queryByText } = render(<JobDetail data={softwareDevData} />)

		expect(getByText('Software Engineer', { selector: 'b' })).not.toBeNull()
		expect(getByText('Google', { exact: false })).not.toBeNull()
		expect(getByText('Description')).not.toBeNull()

		// no value that needs to be shown is missing
		expect(queryByText('undefined', { exact: 'false' })).toBeNull()
	})
})
